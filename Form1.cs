﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KRY_0x04_netcore
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string path = "";

        string sha256sum(string path)
        {
            string sha256sum = "";
            try
            {
                FileStream my_filestream = File.OpenRead(path);
                SHA256Managed my_sha_256_managed = new SHA256Managed();
                byte[] byte_array_of_sha256 = my_sha_256_managed.ComputeHash(my_filestream);
                sha256sum = BitConverter.ToString(byte_array_of_sha256).Replace("-", string.Empty).ToLower();

                my_filestream.Close();
                my_sha_256_managed.Clear();
            }
            catch (FileNotFoundException)
            {
                string itsamessage = path;
                SHA256Managed my_sha_256_managed = new SHA256Managed();
                byte[] byte_array_of_sha256 = my_sha_256_managed.ComputeHash(Encoding.UTF8.GetBytes(itsamessage));
                sha256sum = BitConverter.ToString(byte_array_of_sha256).Replace("-", string.Empty).ToLower();
                my_sha_256_managed.Clear();
            }
            catch (Exception e)
            {
                MessageBox.Show("unreachable error\n https://xkcd.com/2200 \n\nerror:\n" + e.ToString(), "this should never have happened",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return sha256sum;
        }

        string get_file_metrics(string filename)
        {
            string metrics = "";
            metrics += $"created: " + $"{File.GetCreationTime(filename)}" + Environment.NewLine;
            metrics += $"file type: " + $"{Path.GetExtension(filename)}";
            return metrics;
        }

        BigInteger return_bigint_representation_of_message(string input)
        {
            byte[] part_of_message = Encoding.ASCII.GetBytes(input);
            return new BigInteger(part_of_message);
        }

        string return_string_representation_of_bigint(BigInteger bA_number)
        {
            byte[] decrypted_b_a = bA_number.ToByteArray();
            string decrypted_text = "";
            for (int i = 0; i < decrypted_b_a.Length; i++)
            {
                decrypted_text += Convert.ToString(Convert.ToChar(decrypted_b_a[i]));
            }
            return decrypted_text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /* load msg */

            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "pick a msg";
                /* freedom for all */
                dlg.Filter = "message (*.msg)|*.msg|custom (*.*)|*.*";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    richTextBox1.Text = File.ReadAllText(dlg.FileName);
                    textBox1.Text = dlg.FileName;
                    string f = dlg.FileName;
                    textBox4.Text = get_file_metrics(f);
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            /* save msg */

            if (richTextBox1.Text == "")
            {
                MessageBox.Show("you've nothing to dump, friend ()");
                return;
            }
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            saveFileDialog1.Title = "export msg";
            saveFileDialog1.CheckFileExists = false;
            saveFileDialog1.CheckPathExists = false;
            saveFileDialog1.DefaultExt = "";
            saveFileDialog1.Filter = "msg (*.msg)|*.msg|any file you wish (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    File.WriteAllText(saveFileDialog1.FileName, richTextBox1.Text);
                    MessageBox.Show($"saved to {saveFileDialog1.FileName}", "gj, file saved!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error: " + ex.ToString(), "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /* compute sha256 */

            if (textBox1.Text != "")
            {
                textBox3.Text = sha256sum(textBox1.Text);
                textBox3.BorderStyle = BorderStyle.Fixed3D;
            }
            else
            {
                MessageBox.Show("please first load a file, thanks", "why are you doing this to me");
                return;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            /* load privkey */

            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "pick a privkey";
                /* freedom for all */
                dlg.Filter = "private key (*.pri)|*.pri|custom (*.*)|*.*";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    richTextBox2.Text = File.ReadAllText(dlg.FileName);
                    textBox5.Text = dlg.FileName;
                    //path = textBox5.Text;
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            /* load pubkey */

            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "pick a pubkey";
                /* freedom for all */
                dlg.Filter = "public key (*.pub)|*.pub|custom (*.*)|*.*";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    richTextBox3.Text = File.ReadAllText(dlg.FileName);
                    textBox6.Text = dlg.FileName;
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            /* save sha256sum */

            if (textBox3.Text == "")
            {
                MessageBox.Show("trying to dump sha256sum\nyou've nothing to dump, friend", "error: no sha256sum computed");
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            saveFileDialog.Title = "export checksum";
            saveFileDialog.CheckFileExists = false;
            saveFileDialog.CheckPathExists = false;
            saveFileDialog.DefaultExt = "";
            saveFileDialog.Filter = "sha256sum (*.sha256sum)|*.sha256sum|any file you wish (*.*)|*.*";
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    File.WriteAllText(saveFileDialog.FileName, textBox3.Text);
                    MessageBox.Show($"saved to {saveFileDialog.FileName}", "gj, file saved!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error: " + ex.ToString(), "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            /* load sha256 */

            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "pick a sha256sum";
                /* freedom for all */
                dlg.Filter = "sha256sum (*.sha256sum)|*.sha256sum|custom (*.*)|*.*";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    textBox2.Text = File.ReadAllText(dlg.FileName);
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            /* zip */
            if (textBox1.Text == "")
            {
                MessageBox.Show("load a msg file first, please", "oh hey");
                return;
            }
            using (FolderBrowserDialog dlg = new FolderBrowserDialog())
            {
                dlg.Description = "choose a folder to zip";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    string pathTo_dirToZip = dlg.SelectedPath;
                    using (SaveFileDialog dlggg = new SaveFileDialog())
                    {
                        dlggg.Title = "where to save sha256sum of earlier specified msg";
                        dlggg.Filter = "sha256sum (*.sha256sum)|*.sha256sum|sh256sum in windows-recognizable text file ending with \"txt\" (*.txt)|*.txt|custom (*.*)|*.*";
                        if (dlggg.ShowDialog() == DialogResult.OK)
                        {
                            string path_to_sha256sum = dlggg.FileName;
                            // VYTVORENI ADRESARE dirToZip
                            if (path_to_sha256sum == "")
                            {
                                MessageBox.Show("please specify a path first", "not this again, come on");
                                return;
                            }
                            Directory.CreateDirectory(pathTo_dirToZip);

                            // VYTVORENI/ZAPIS SOUBORU sha256OfMessage.txt DO ADRESARE dirToZip
                            // OBSAHEM SOUBORU sha256OfMessage.txt je SHA256 OTISK SOUBORU, KTERY SE NACHAZI NA CESTE pathToMessageForTransfer
                            File.WriteAllText(path_to_sha256sum, sha256sum(textBox1.Text));

                            try
                            {
                                if (File.Exists(pathTo_dirToZip + "zipfile.zip"))
                                {
                                    MessageBox.Show($"file exists: {pathTo_dirToZip + "zipfile.zip"}\ndeleting", "ayy");
                                    File.Delete(pathTo_dirToZip + "zipfile.zip");
                                }

                                ZipFile.CreateFromDirectory(pathTo_dirToZip, pathTo_dirToZip + "zipfile.zip");
                                MessageBox.Show("great success!", "gj, zip created");
                            }
                            catch (IOException exception)
                            {
                                MessageBox.Show("error: " + exception.ToString(), "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
        }
        private void button10_Click(object sender, EventArgs e)
        {
            /* unzip */
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "choose a zip file to unzip";
                dlg.Filter = "zip (*.zip)|*.zip|custom (*.*)|*.*";
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    string zipfile = dlg.FileName;
                    using (FolderBrowserDialog dlgg = new FolderBrowserDialog())
                    {
                        dlgg.Description = "select folder to unzip the zip to yeeeaaah";
                        if (dlgg.ShowDialog() == DialogResult.OK)
                        {
                            string pathTo_UnzippedDir = dlgg.SelectedPath;
                            path = pathTo_UnzippedDir;

                            if (Directory.Exists(pathTo_UnzippedDir))
                            {
                                pathTo_UnzippedDir = pathTo_UnzippedDir + "-NEW-" + DateTime.Now.ToString("yyyyMMddTHHmmss");
                                MessageBox.Show("dir exists, created a new one with the same name + date", "just so you know");
                                Directory.CreateDirectory(pathTo_UnzippedDir);
                            }

                            try
                            {
                                ZipFile.ExtractToDirectory(zipfile, pathTo_UnzippedDir);
                                MessageBox.Show("great success!", "gj, archive unzipped");
                            }
                            catch (IOException exc)
                            {
                                MessageBox.Show("error: " + exc.ToString(), "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            /* sign */
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (richTextBox2.Text == "")
            {
                MessageBox.Show("you haven't specified a private key to use for signing process", "a minor nag - missing privkey", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string[] keys = richTextBox2.Text.Split(' ');
            MessageBox.Show("we good?");
            Console.WriteLine(BigInteger.Parse(keys[1]));
            Console.WriteLine(BigInteger.Parse(keys[0]));
            if (richTextBox1.Text == "")
            {
                MessageBox.Show("you are signing an empty message, just so you know", "a minor nag - empty message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            string signed = Convert.ToString(BigInteger.ModPow(return_bigint_representation_of_message(richTextBox1.Text), BigInteger.Parse(keys[1]), BigInteger.Parse(keys[0])));


            saveFileDialog1.Title = ".sign";
            saveFileDialog1.CheckFileExists = false;
            saveFileDialog1.CheckPathExists = false;
            saveFileDialog1.DefaultExt = "";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    File.WriteAllText(saveFileDialog1.FileName, signed);
                    MessageBox.Show($"saved to {saveFileDialog1.FileName}", "gj, file saved!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error: " + ex.ToString(), "this didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            /* verify */
            if (richTextBox3.Text == "")
            {
                MessageBox.Show("you haven't specified a public key to use for verification process", "a minor nag - missing pubkey", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string[] keys = richTextBox3.Text.Split(' ');
            string verified = "";
            string orig_message = "";

            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "pick a .sign file";
                /* freedom for all */
                dlg.Filter = "sign file (*.sign)|*.sign|custom (*.*)|*.*";
                //MessageBox.Show($"we still good\ndlg was{dlg}");

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        verified = return_string_representation_of_bigint(BigInteger.ModPow(BigInteger.Parse(File.ReadAllText(dlg.FileName)), BigInteger.Parse(keys[1]), BigInteger.Parse(keys[0])));
                    }
                    catch (Exception excep)
                    {
                        MessageBox.Show("error: " + excep.ToString(), "this really didn't work...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("you have to choose a sign file for this operation", "just so you know..", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            if (textBox1.Text == "" && richTextBox1.Text != "")
            {
                Color origcol = richTextBox1.BackColor;
                richTextBox1.BackColor = Color.AliceBlue;
                DialogResult msgresult = MessageBox.Show("Use contents of textbox \"message\" as a message file?", "");
                if (msgresult == DialogResult.OK)
                {
                    MessageBox.Show("using message from textbox");
                    richTextBox1.BackColor = origcol;
                    orig_message = richTextBox1.Text;
                }
                richTextBox1.BackColor = origcol;
            }
            else if (textBox1.Text != "")
            {
                try
                {
                    orig_message = File.ReadAllText(textBox1.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("error:\n" + ex.ToString(), "this didn't work out so well...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    MessageBox.Show("you'll need to specify another message file",$"there was an error reading the file \"{orig_message}\"");
                    return;
                }
            }
            else
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    dlg.Title = "pick a .msg file";
                    /* freedom for all */
                    dlg.Filter = "msg file (*.msg)|*.msg|custom (*.*)|*.*";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            orig_message = File.ReadAllText(dlg.FileName);
                        }
                        catch (Exception excep)
                        {
                            MessageBox.Show("error: " + excep.ToString(), "this didn't work out...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("no message file was specified\n" +
                            "a message file is required for this operation", "just so you know..", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            if (sha256sum(orig_message) == sha256sum(verified))
            {
                MessageBox.Show($"{sha256sum(orig_message)} == {sha256sum(verified)}", "we good, checksums match!");
            }
            else
            {
                Console.WriteLine($"orig message: {orig_message}\nsha256sum: {sha256sum(orig_message)}\n\n");
                Console.WriteLine($"verif message: {verified}\nsha256sum: {(sha256sum(verified))}\n\n");
                MessageBox.Show($"something is seriously wrong\n\norig: {orig_message}\nsha256sum: {sha256sum(orig_message)}\n\n"
                    + $"verified: {verified}\nsha256sum: {sha256sum(verified)}\n\n", "checksums differ!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            richTextBox1.Text = "";
            richTextBox2.Text = "";
            richTextBox3.Text = "";
            path = "";
        }

        private void button14_Click(object sender, EventArgs e)
        {
            /* keysize button */
            rsa nursa = new rsa();
            nursa.keysize_handler(button14);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            rsa nursa = new rsa();
            nursa.genprimes_handler(textBox7, textBox8, textBox10, textBox9, label14);
        }
    }
}
